module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      files: ['Gruntfile.js', 'src/app/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },

    concat: {
      options: {
      },
      dist: {
        src: ['src/app/**/*.js'],
        // the location of the resulting JS file
        dest: 'dist/<%= pkg.name %>.js'
      }
    },

    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },

    injector: {
      options: {
        addRootSlash: false,
		transform: function(filePath) {
			
			filePath = filePath.replace('src/', '');
			filePath = filePath.replace('dist/', '');
			if (filePath.endsWith('.css')) {
				return '<link rel="stylesheet" href="'+filePath+'">';
			} else {
				return '<script src="'+filePath+'"></script>';
			}
			
		},
      },
      local_dependencies: {
        files: {
          'src/index.html': ['src/app/**/*.js', 'src/css/**/*.css'],
          'dist/index.html': ['dist/galaxus.min.js', 'dist/css/**/*.css'],
        }
      }            
    },

    wiredep: {
      target: {
        src: 'src/index.html',
        //ignorePath: '<%= yeoman.client %>/',
        //exclude: [/bootstrap-sass-official/, /bootstrap.js/, '/json3/', '/es5-shim/', /bootstrap.css/, /font-awesome.css/ ]
      }
    },

    bower: {
      dev: {
        dest: 'dist/bower_components',
        options: {
          expand: true
        }
      }
    },

    less: {
      development: {
        options: {
          paths: ['src/css']
        },
        files: {'src/css/main.css':'src/css/main.less'}
      },
      production: {
        options: {
          paths: ['src/css']
        },
        files: {'src/css/main.css':'src/css/main.less'}
      }
    },

    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint']
    },

    copy: {
      main: {
        files: [
          {src: ['src/index.html'], dest: 'dist/index.html'},
          {src: ['src/css/main.css'], dest: 'dist/css/main.css'}
        ],
      },
    },

    clean: {
      src: [ 'dist' ]
    }

  });

  //less
  grunt.loadNpmTasks('grunt-contrib-less');
  
  //javascript
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-injector');

  //bower dependencies
  grunt.loadNpmTasks('grunt-wiredep');
  grunt.loadNpmTasks('grunt-bower');

  //system
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
 
  grunt.registerTask('default', [
    'clean',
    'less',
    'jshint','concat','uglify',
    'wiredep','bower',
    'copy',
    'injector'
    ]);

};