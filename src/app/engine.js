(function() {
	App.Engine = function() {
		var self = this;
		
		var _scene;
		var _camera;
		var _renderer;
		
		var _stats;
		var _tics = 0;
		
		var _sun = new App.Body();
		var _earth = new App.Body();
		var _venus = new App.Body();
		
		var _bodies = [];
		
		var _clock = new THREE.Clock();
		
		this.init = function() {
			_renderer = new THREE.WebGLRenderer({antialias:true});
			_renderer.setSize( window.innerWidth, window.innerHeight );
			document.body.appendChild( _renderer.domElement );
			
			window.addEventListener( 'resize', this.onWindowResize, false );
			
			_scene = new THREE.Scene();
			_camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
			_scene.add( new THREE.AmbientLight( 0xeef0ff ) );
			
			var light1 = new THREE.PointLight( 0xffffff, 2, 20 );
			light1.position.set(0, 0, 0);
			_scene.add( light1 );
			
			_sun.init('SUN',10,'wireframeY');
			_sun.mass = 1.98892 *  Math.pow(10,30);

			_earth.init('EARTH',0.1,'wireframeB');
			_earth.mass = 5.9742 * Math.pow(10,24);
			_earth.px = 1*AU;
			_earth.vy = -29.783 * 1000;  //29.783 km/sec

			_venus.init('Venus',0.1,'wireframeR');
			_venus.mass = 4.8685 * Math.pow(10,24);
			_venus.px = 0.723 * AU;
			_venus.vy = -35.02 * 1000;
			
			_scene.add(_sun.getMesh());
			_scene.add(_earth.getMesh());
			_scene.add(_venus.getMesh());
			
			_bodies = [_sun,_earth,_venus];

			_camera.position.z = 400;
		};
		
		this.onWindowResize = function() {
			_camera.aspect = window.innerWidth / window.innerHeight;
			_camera.updateProjectionMatrix();
			_renderer.setSize(window.innerWidth, window.innerHeight);
		};
		
		var timestep = 24*3600;
		
		this.sceneLoop = function() {
			
			timestep ++;
			
			for(var i=0;i<_bodies.length;i++) {
				//Add up all of the forces exerted on 'body'.
				var total_fx = 0.0;
				var total_fy = 0.0;
				for(var j=0;j<_bodies.length;j++) {
					//Don't calculate the body's attraction to itself
					if (_bodies[i]!==_bodies[j]) {
						var forces = _bodies[i].attraction(_bodies[j]);
						total_fx += forces[0];
						total_fy += forces[1];
					}
					
					_bodies[i].fx = total_fx;
					_bodies[i].fy = total_fy;
				}
			}
			
			for(var k=0;k<_bodies.length;k++) {
				var body = _bodies[k];
				body.vx += body.fx / body.mass * timestep;
				body.vy += body.fy / body.mass * timestep;

				//# Update positions
				body.px += body.vx * timestep;
				body.py += body.vy * timestep;
				
				body.getMesh().position.x = body.px * SCALE;
				body.getMesh().position.y = body.py * SCALE;
			}
		};
		
		this.render = function() {
			requestAnimationFrame(self.render);
			
			_tics += _clock.getDelta();
			
			if (_tics>=1/30) {
				self.sceneLoop();
				_tics = 0;
			}
			
			_renderer.render(_scene, _camera);
			//_stats.update();
		};
	};
})();

