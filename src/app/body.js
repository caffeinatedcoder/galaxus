(function() {
	
	App.Body = function() {
		var self = this;
		
		self.name = undefined;
		self.mass = undefined;
		self.vx = 0.0;
		self.vy = 0.0;
		self.px = 0.0;
		self.py = 0.0;
		self.fx = 0.0;
		self.fy = 0.0;
		
		var _material;
		var _geometry;
		var _mesh;
		
		var _text;
		
		this.init = function(name,size,materialId) {
			
			self.name = name;
			
			_geometry = new THREE.IcosahedronGeometry(size, 2);
			_material = App.materials[materialId];
			
			_mesh = new THREE.Mesh(_geometry, _material);
			
			_text = document.createElement( 'div' );
			_text.className = 'billboard';
			_text.style.position = 'absolute';
			_text.innerHTML = name;
			document.body.appendChild(_text);
			
			return self;
		};
		
		this.attraction = function(other) {
			//Compute the distance of the other body
			var sx = self.px;
			var sy = self.py;
			var ox = other.px;
			var oy = other.py;
			
			var dx = (ox-sx);
			var dy = (oy-sy);
			var d = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));
			
			 if (d === 0) {
				 console.log('Collision between objects');
				 return [0,0];
			 }
			
			//Compute the force of attraction
			var f = G * self.mass * other.mass / Math.pow(d,2);

			//Compute the direction of the force.
			var theta = Math.atan2(dy, dx);
			var fx = Math.cos(theta) * f;
			var fy = Math.sin(theta) * f;
			return [fx, fy];
		};
		
		this.getMesh = function() {
			return _mesh;
		};
		
		this.tic = function(camera,renderer) {
			var coord =  App.toScreenXY(_mesh,camera,renderer);
			_text.innerHTML = name;
			_text.style.left = coord.x + 'px';
			_text.style.top = coord.y + 'px';
		};
	};
	
})();

