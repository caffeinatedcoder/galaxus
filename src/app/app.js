(function() {	
	App.materials = {
			lambertDefault: new THREE.MeshLambertMaterial({color:0x666666,wireframe:false}),
			wireframe: new THREE.MeshLambertMaterial({color:0x666666,wireframe:true}),
			wireframeR: new THREE.MeshLambertMaterial({color:0x990000,wireframe:true}),
			wireframeG: new THREE.MeshLambertMaterial({color:0x009900,wireframe:true}),
			wireframeB: new THREE.MeshLambertMaterial({color:0x000099,wireframe:true}),
			wireframeY: new THREE.MeshLambertMaterial({color:0x999900,wireframe:true})
	};
	
	App.radians = function(degrees) {
		return degrees * Math.PI / 180;
	};
 
	App.degrees = function(radians) {
	  return radians * 180 / Math.PI;
	};
	
	App.toScreenXY = function(obj,camera,renderer) {
		var vector = new THREE.Vector3();

		var widthHalf = 0.5*renderer.context.canvas.width;
		var heightHalf = 0.5*renderer.context.canvas.height;

		obj.updateMatrixWorld();
		vector.setFromMatrixPosition(obj.matrixWorld);
		vector.project(camera);

		vector.x = ( vector.x * widthHalf ) + widthHalf;
		vector.y = - ( vector.y * heightHalf ) + heightHalf;

		return { 
			x: vector.x,
			y: vector.y
		};
	};
	
	App.createOrbit = function(segmentCount,radius,material) {
		var geometry = new THREE.Geometry();
		material = new THREE.LineBasicMaterial({ color: 0x666666 });

		for (var i = 0; i <= segmentCount; i++) {
			var theta = (i / segmentCount) * Math.PI * 2;
			geometry.vertices.push(
				new THREE.Vector3(
					Math.cos(theta) * radius,
					Math.sin(theta) * radius,
					0));            
		}
		
		return new THREE.Line(geometry, material);
	};
	
	App.createEllipticalOrbit = function(a,b) {
		console.log('App.createEllipticalOrbit', Math.PI*2);
		var geometry = new THREE.Geometry();
		material = new THREE.LineBasicMaterial({ color: 0x666666 });
		
		for (var i = 0; i < Math.PI*2; i+=0.1) {
			console.log(a*Math.cos(i),b*Math.sin(i));
			geometry.vertices.push(
				new THREE.Vector3(
					a*Math.cos(i),
					b*Math.sin(i),
					0));            
		}
		
		geometry.vertices.push(
			new THREE.Vector3(
				a*Math.cos(Math.PI*2),
				b*Math.sin(Math.PI*2),
				0));            
		
		return new THREE.Line(geometry, material);
	};
	
})();

