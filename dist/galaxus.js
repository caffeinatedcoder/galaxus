(function() {	
	App.materials = {
			lambertDefault: new THREE.MeshLambertMaterial({color:0x666666,wireframe:false}),
			wireframe: new THREE.MeshLambertMaterial({color:0x666666,wireframe:true}),
			wireframeR: new THREE.MeshLambertMaterial({color:0x990000,wireframe:true}),
			wireframeG: new THREE.MeshLambertMaterial({color:0x009900,wireframe:true}),
			wireframeB: new THREE.MeshLambertMaterial({color:0x000099,wireframe:true}),
			wireframeY: new THREE.MeshLambertMaterial({color:0x999900,wireframe:true})
	};
	
	App.radians = function(degrees) {
		return degrees * Math.PI / 180;
	};
 
	App.degrees = function(radians) {
	  return radians * 180 / Math.PI;
	};
	
	App.toScreenXY = function(obj,camera,renderer) {
		var vector = new THREE.Vector3();

		var widthHalf = 0.5*renderer.context.canvas.width;
		var heightHalf = 0.5*renderer.context.canvas.height;

		obj.updateMatrixWorld();
		vector.setFromMatrixPosition(obj.matrixWorld);
		vector.project(camera);

		vector.x = ( vector.x * widthHalf ) + widthHalf;
		vector.y = - ( vector.y * heightHalf ) + heightHalf;

		return { 
			x: vector.x,
			y: vector.y
		};
	};
	
	App.createOrbit = function(segmentCount,radius,material) {
		var geometry = new THREE.Geometry();
		material = new THREE.LineBasicMaterial({ color: 0x666666 });

		for (var i = 0; i <= segmentCount; i++) {
			var theta = (i / segmentCount) * Math.PI * 2;
			geometry.vertices.push(
				new THREE.Vector3(
					Math.cos(theta) * radius,
					Math.sin(theta) * radius,
					0));            
		}
		
		return new THREE.Line(geometry, material);
	};
	
	App.createEllipticalOrbit = function(a,b) {
		console.log('App.createEllipticalOrbit', Math.PI*2);
		var geometry = new THREE.Geometry();
		material = new THREE.LineBasicMaterial({ color: 0x666666 });
		
		for (var i = 0; i < Math.PI*2; i+=0.1) {
			console.log(a*Math.cos(i),b*Math.sin(i));
			geometry.vertices.push(
				new THREE.Vector3(
					a*Math.cos(i),
					b*Math.sin(i),
					0));            
		}
		
		geometry.vertices.push(
			new THREE.Vector3(
				a*Math.cos(Math.PI*2),
				b*Math.sin(Math.PI*2),
				0));            
		
		return new THREE.Line(geometry, material);
	};
	
})();


(function() {
	
	App.Body = function() {
		var self = this;
		
		self.name = undefined;
		self.mass = undefined;
		self.vx = 0.0;
		self.vy = 0.0;
		self.px = 0.0;
		self.py = 0.0;
		self.fx = 0.0;
		self.fy = 0.0;
		
		var _material;
		var _geometry;
		var _mesh;
		
		var _text;
		
		this.init = function(name,size,materialId) {
			
			self.name = name;
			
			_geometry = new THREE.IcosahedronGeometry(size, 2);
			_material = App.materials[materialId];
			
			_mesh = new THREE.Mesh(_geometry, _material);
			
			_text = document.createElement( 'div' );
			_text.className = 'billboard';
			_text.style.position = 'absolute';
			_text.innerHTML = name;
			document.body.appendChild(_text);
			
			return self;
		};
		
		this.attraction = function(other) {
			//Compute the distance of the other body
			var sx = self.px;
			var sy = self.py;
			var ox = other.px;
			var oy = other.py;
			
			var dx = (ox-sx);
			var dy = (oy-sy);
			var d = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));
			
			 if (d === 0) {
				 console.log('Collision between objects');
				 return [0,0];
			 }
			
			//Compute the force of attraction
			var f = G * self.mass * other.mass / Math.pow(d,2);

			//Compute the direction of the force.
			var theta = Math.atan2(dy, dx);
			var fx = Math.cos(theta) * f;
			var fy = Math.sin(theta) * f;
			return [fx, fy];
		};
		
		this.getMesh = function() {
			return _mesh;
		};
		
		this.tic = function(camera,renderer) {
			var coord =  App.toScreenXY(_mesh,camera,renderer);
			_text.innerHTML = name;
			_text.style.left = coord.x + 'px';
			_text.style.top = coord.y + 'px';
		};
	};
	
})();


(function() {
	App.Engine = function() {
		var self = this;
		
		var _scene;
		var _camera;
		var _renderer;
		
		var _stats;
		var _tics = 0;
		
		var _sun = new App.Body();
		var _earth = new App.Body();
		var _venus = new App.Body();
		
		var _bodies = [];
		
		var _clock = new THREE.Clock();
		
		this.init = function() {
			_renderer = new THREE.WebGLRenderer({antialias:true});
			_renderer.setSize( window.innerWidth, window.innerHeight );
			document.body.appendChild( _renderer.domElement );
			
			window.addEventListener( 'resize', this.onWindowResize, false );
			
			_scene = new THREE.Scene();
			_camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
			_scene.add( new THREE.AmbientLight( 0xeef0ff ) );
			
			var light1 = new THREE.PointLight( 0xffffff, 2, 20 );
			light1.position.set(0, 0, 0);
			_scene.add( light1 );
			
			_sun.init('SUN',10,'wireframeY');
			_sun.mass = 1.98892 *  Math.pow(10,30);

			_earth.init('EARTH',0.1,'wireframeB');
			_earth.mass = 5.9742 * Math.pow(10,24);
			_earth.px = 1*AU;
			_earth.vy = -29.783 * 1000;  //29.783 km/sec

			_venus.init('Venus',0.1,'wireframeR');
			_venus.mass = 4.8685 * Math.pow(10,24);
			_venus.px = 0.723 * AU;
			_venus.vy = -35.02 * 1000;
			
			_scene.add(_sun.getMesh());
			_scene.add(_earth.getMesh());
			_scene.add(_venus.getMesh());
			
			_bodies = [_sun,_earth,_venus];

			_camera.position.z = 400;
		};
		
		this.onWindowResize = function() {
			_camera.aspect = window.innerWidth / window.innerHeight;
			_camera.updateProjectionMatrix();
			_renderer.setSize(window.innerWidth, window.innerHeight);
		};
		
		var timestep = 24*3600;
		
		this.sceneLoop = function() {
			
			timestep ++;
			
			for(var i=0;i<_bodies.length;i++) {
				//Add up all of the forces exerted on 'body'.
				var total_fx = 0.0;
				var total_fy = 0.0;
				for(var j=0;j<_bodies.length;j++) {
					//Don't calculate the body's attraction to itself
					if (_bodies[i]!==_bodies[j]) {
						var forces = _bodies[i].attraction(_bodies[j]);
						total_fx += forces[0];
						total_fy += forces[1];
					}
					
					_bodies[i].fx = total_fx;
					_bodies[i].fy = total_fy;
				}
			}
			
			for(var k=0;k<_bodies.length;k++) {
				var body = _bodies[k];
				body.vx += body.fx / body.mass * timestep;
				body.vy += body.fy / body.mass * timestep;

				//# Update positions
				body.px += body.vx * timestep;
				body.py += body.vy * timestep;
				
				body.getMesh().position.x = body.px * SCALE;
				body.getMesh().position.y = body.py * SCALE;
			}
		};
		
		this.render = function() {
			requestAnimationFrame(self.render);
			
			_tics += _clock.getDelta();
			
			if (_tics>=1/30) {
				self.sceneLoop();
				_tics = 0;
			}
			
			_renderer.render(_scene, _camera);
			//_stats.update();
		};
	};
})();

