# GALAXUS #

This is a work in progress test of the webgl features using three.js

## view online ##

First Test - just translations and rotations by the three.js:
(http://www.lodevalm.net/webgl/galaxus/)

Second Test - Kepler elliptical orbit
(http://www.lodevalm.net/webgl/galaxus2/)

Third Test: Newton's gravitational law
(http://www.lodevalm.net/webgl/galaxus3/)

## links ##
https://fiftyexamples.readthedocs.org/en/latest/gravity.html#code-discussion